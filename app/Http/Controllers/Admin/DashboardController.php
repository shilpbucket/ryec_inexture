<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Auth;
use Input;
use Redirect;
use App\Business;
use App\Notification;
use Cache;
use Illuminate\Contracts\Encryption\DecryptException;

class DashboardController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
        $this->objBusiness = new Business();
    }

    public function index()
    {
        if (false){
            $membersForApproval = Cache::get('membersForApproval');
        } else {
            $filters = [
                'approved' => 0
            ];
            $membersForApproval = $this->objBusiness->getAll($filters);
            Cache::put('membersForApproval', $membersForApproval, 60);
        }

        return view('Admin.Dashboard', compact('membersForApproval'));
    }

    public function getLogout()
    {
        Auth::logout();

        return Redirect::to('admin/login');
    }

    public function siteAnalytics()
    {
        return view('Admin.Analytics');
    }

    public function notifications()
    {
        return view('Admin.ListNotifications');
    }

    public function sendNotification($type) {
        if($type != '') {
            if($type == 'rajputbusinessregister') {
                $message = "Dear [FULL_NAME],  Join the Rajput Business revolution by registering your business / profession with Rajput Yuva Entrepreneur Club.";
            } elseif($type == 'upgradetopremium') {
                $message = "Dear [FULL_NAME],  Give edge to your business with Premium Memebship. You are missing out on great features of RYEC. Upgrade to premium membership to enhance your business."; 
            }

            $response = Notification::create([
                'notification_type' => $type,
                'chennel_type' => 'push',
                'message' => $message            
            ]);

            return redirect()->back()->with('success', 'Notification sent successfully');
        } else {
            return redirect()->back()->withErrors(['Invalid Notification']);
        }
    }
}
