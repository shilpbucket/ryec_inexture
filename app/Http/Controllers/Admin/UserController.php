<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests\UserRequest;
use App\Http\Requests\BusinessRequest;
use Auth;
use Input;
use Config;
use Redirect;
use App\User;
use App\Business;
use App\UserRole;
use App\UserMetaData;
use App\Http\Controllers\Controller;
use Crypt;
use Helpers;
use Image;
use Illuminate\Contracts\Encryption\DecryptException;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->objUser = new User();
        $this->objUserMetaData = new UserMetaData();
        $this->objUserRole = new UserRole();
        $this->objBusiness = new Business();
        $this->BUSINESS_BANNER_IMAGE_PATH = Config::get('constant.BUSINESS_BANNER_IMAGE_PATH');
        $this->USER_ORIGINAL_IMAGE_PATH = Config::get('constant.USER_ORIGINAL_IMAGE_PATH');
        $this->USER_THUMBNAIL_IMAGE_PATH = Config::get('constant.USER_THUMBNAIL_IMAGE_PATH');
        $this->USER_THUMBNAIL_IMAGE_HEIGHT = Config::get('constant.USER_THUMBNAIL_IMAGE_HEIGHT');
        $this->USER_THUMBNAIL_IMAGE_WIDTH = Config::get('constant.USER_THUMBNAIL_IMAGE_WIDTH');
        
        $this->loggedInUser = Auth::guard();
        $this->log = new Logger('user-controller');
        $this->log->pushHandler(new StreamHandler(storage_path().'/logs/monolog-'.date('m-d-Y').'.log'));
    }

    public function index()
    {   
        $postData = Input::get();

        if((isset($postData['searchtext']) && $postData['searchtext'] != '') || (isset($postData['usertype']) && $postData['usertype'] != ''))
        {   
            $this->log->info('Admin user listing page', array('admin_user_id' => Auth::id()));
            $userList = $this->objUser->userFilter($postData);
        }
        else
        {
            $this->log->info('Admin user listing page', array('admin_user_id' => Auth::id()));
            $userList = $this->objUser->getAll([],true);
        }
        
        return view('Admin.ListUsers', compact('userList','postData'));
    }

    public function add()
    {
        $this->log->info('Admin user add page', array('admin_user_id' => Auth::id()));
        return view('Admin.EditUser');
    }

    public function addAgent()
    {
        $agent_approved = '1';
        $this->log->info('Admin agent add page', array('admin_user_id' => Auth::id()));
        return view('Admin.EditUser',compact('agent_approved'));
    }

    public function save(UserRequest $request)
    {
        $postData = Input::get();

        unset($postData['_token']);
        
        if(isset($postData['password']) && $postData['password'] != '') 
        {
            $postData['password'] = bcrypt($postData['password']);
        } else {
            unset($postData['password']);
        }
        
        // upload user profile picture
        if (Input::file('profile_pic')) 
        {  
            $profile_pic = Input::file('profile_pic');

            if (!empty($profile_pic) && count($profile_pic) > 0) 
            {
                $fileName = 'user_' . uniqid() . '.' . $profile_pic->getClientOriginalExtension();

                $pathOriginal = public_path($this->USER_ORIGINAL_IMAGE_PATH . $fileName);
                $pathThumb = public_path($this->USER_THUMBNAIL_IMAGE_PATH . $fileName);

                Image::make($profile_pic->getRealPath())->save($pathOriginal);
                Image::make($profile_pic->getRealPath())->resize($this->USER_THUMBNAIL_IMAGE_WIDTH, $this->USER_THUMBNAIL_IMAGE_HEIGHT)->save($pathThumb);

                if(isset($postData['old_profile_pic']) && $postData['old_profile_pic'] != '')
                {
                    $originalImageDelete = Helpers::deleteFileToStorage($postData['old_profile_pic'], $this->USER_ORIGINAL_IMAGE_PATH, "s3");
                    $thumbImageDelete = Helpers::deleteFileToStorage($postData['old_profile_pic'], $this->USER_THUMBNAIL_IMAGE_PATH, "s3");
                }

                //Uploading on AWS
    			$originalImage = Helpers::addFileToStorage($fileName, $this->USER_ORIGINAL_IMAGE_PATH, $pathOriginal, "s3");
    			$thumbImage = Helpers::addFileToStorage($fileName, $this->USER_THUMBNAIL_IMAGE_PATH, $pathThumb, "s3");
    			//Deleting Local Files
    			\File::delete($this->USER_ORIGINAL_IMAGE_PATH . $fileName);
    			\File::delete($this->USER_THUMBNAIL_IMAGE_PATH . $fileName);

                $postData['profile_pic'] = $fileName;
            }
        }
        
        $subscription =  (!isset($postData['subscription'])) ? 0 : 1;
        $postData['subscription'] = $subscription;

        $isRajput =  (!isset($postData['isRajput'])) ? 0 : 1;
        $postData['isRajput'] = $isRajput;
        
        $response = $this->objUser->insertUpdate($postData);
        $userId = (isset($postData['id']) && $postData['id'] == 0) ? $response->id : $postData['id'];
        if ($response) 
        {            
            UserRole::firstOrCreate(['user_id' =>  ($postData['id'] == 0)?$response->id:$postData['id'], 'role_id' => Config::get('constant.USER_ROLE_ID')]);
            if(isset($postData['agent_approved']) && $postData['agent_approved'] == 1)
            {
                $this->log->info('Admin user added/updated successfully', array('admin_user_id' =>  Auth::id(), 'user_id' => $userId));
                return Redirect::to("admin/users")->with('success', trans('labels.agentsuccessmsg'));
            }
            else
            {
                $this->log->info('Admin user added/updated successfully', array('admin_user_id' =>  Auth::id(), 'user_id' => $userId));
                return Redirect::to("admin/users")->with('success', trans('labels.usersuccessmsg'));
            }
        } else {
            $this->log->error('Admin something went wrong while adding/updating user', array('admin_user_id' =>  Auth::id(), 'user_id' => $userId));
            return Redirect::to("admin/users")->with('error', trans('labels.usererrormsg'));
        }
    }

    public function edit($id)
    {
        try 
        {
            $id = Crypt::decrypt($id);
            $data = $this->objUser->find($id);
            $isVendor = Helpers::userIsVendorOrNot($id);
            
            if($data) 
            {
                $this->log->info('Admin user edit page', array('admin_user_id' =>  Auth::id(), 'user_id' => $id));
                return view('Admin.EditUser', compact('data','isVendor'));
            } else {
                $this->log->error('Admin something went wrong while user edit page', array('admin_user_id' =>  Auth::id(), 'user_id' => $id));
                return Redirect::to("admin/users")->with('error', trans('labels.recordnotexist'));
            }
        } catch (DecryptException $e) {
            $this->log->error('Admin something went wrong while user edit page', array('admin_user_id' =>  Auth::id(), 'user_id' => $id, 'error' => $e->getMessage()));
            return view('errors.404');
        }
                
        
    }

    public function editAgent($id)
    {
        try 
        {
            $id = Crypt::decrypt($id);
            $data = $this->objUser->find($id);
            $isVendor = Helpers::userIsVendorOrNot($id);
           
            if($data) 
            {
                $this->log->info('Admin user edit agent page', array('admin_user_id' =>  Auth::id(), 'user_id' => $id));
                return view('Admin.EditUser', compact('data','isVendor'));
            } else {
                $this->log->error('Admin something went wrong while agent edit page', array('admin_user_id' =>  Auth::id(), 'user_id' => $id));
                return Redirect::to("admin/users")->with('error', trans('labels.recordnotexist'));
            }
        }   
        catch (DecryptException $e) 
        {
            $this->log->error('Admin something went wrong while agent edit page', array('admin_user_id' =>  Auth::id(), 'user_id' => $id, 'error' => $e->getMessage()));
            return view('errors.404');
        }
    }

    public function delete($id)
    {
        $data = $this->objUser->find($id);
        $response = $data->delete();
        if ($response) 
        {
            $this->log->info('Admin user deleted successfully', array('admin_user_id' => Auth::id(),'user_id' => $id));
            return Redirect::to("admin/users")->with('success', trans('labels.userdeletesuccessmsg'));
        }
    } 

    public function setUserActive($id)
    {
        $data = $this->objUser->withTrashed()->find($id);
        $data->deleted_at = NULL;
        $response = $data->save();
        if ($response) 
        {
           return 1;
        }
    }

    



}
