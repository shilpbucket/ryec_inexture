<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Cms;
use App\Http\Requests\CmsRequest;
use Auth;
use Input;
use Config;
use Redirect;
use Crypt;
use Illuminate\Contracts\Encryption\DecryptException;

class CmsController extends Controller
{
    public function __construct()
    { 
        $this->middleware('auth');
        $this->objCms = new Cms();
    }

    public function index()
    {
        $cmsList = $this->objCms->getAll();
        return view('Admin.ListCms', compact('cmsList'));
    }

    public function add()
    {
        $data = [];
        return view('Admin.EditCms', compact('data'));
    }

    public function edit($id)
    {
        try {
            $id = Crypt::decrypt($id);
            $data = $this->objCms->find($id);
            if($data) {
                return view('Admin.EditCms', compact('data'));
            } else {
                return Redirect::to("admin/cms")->with('error', trans('labels.recordnotexist'));
            }
        } catch (DecryptException $e) {
            return view('errors.404');
        }
        
    }

    public function save(CmsRequest $request)
    {
        $postData = Input::get();
        unset($postData['_token']);
        $response = $this->objCms->insertUpdate($postData);
        if ($response) {
            return Redirect::to("admin/cms")->with('success', trans('labels.cmssuccessmsg'));
        } else {
            return Redirect::to("admin/cms")->with('error', trans('labels.cmserrormsg'));
        }
    }

    public function delete($id)
    {
        $data = $this->objCms->find($id);
        $response = $data->delete();
        if ($response) {
            return Redirect::to("admin/cms")->with('success', trans('labels.cmsdeletesuccessmsg'));
        }
    }

}
