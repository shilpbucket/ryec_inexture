<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\CategoryRequest;
use Auth;
use Helpers;
use Config;
use Image;
use File;
use DB;
use Input;
use Redirect;
use App\Chats;
use App\ChatMessages;
use App\User;
use App\UsersDevice;
use App\UserRole;
use App\UserMetaData;
use App\Category;
use App\Business;
use App\BusinessImage;
use Crypt;
use Response;
use Carbon\Carbon;
use Mail;
use Session;
use Cache;
use Validator;
use JWTAuth;
use JWTAuthException;
use \stdClass;
use Storage;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;

class ChatsController extends Controller
{
    public function __construct()
    {
        $this->objChats = new Chats();
        $this->objChatMessages = new ChatMessages();
        $this->objCategory = new Category();
        $this->objBusiness = new Business(); 
        $this->objBusinessImage = new BusinessImage();
        $this->objUser = new User();
        $this->objUsersDevice = new UsersDevice();
        $this->objUserMetaData = new UserMetaData();
        $this->objUserRole = new UserRole();
        
        $this->BUSINESS_ORIGINAL_IMAGE_PATH = Config::get('constant.BUSINESS_ORIGINAL_IMAGE_PATH');
        $this->BUSINESS_THUMBNAIL_IMAGE_PATH = Config::get('constant.BUSINESS_THUMBNAIL_IMAGE_PATH');
        
        $this->USER_ORIGINAL_IMAGE_PATH = Config::get('constant.USER_ORIGINAL_IMAGE_PATH');
        $this->USER_THUMBNAIL_IMAGE_PATH = Config::get('constant.USER_THUMBNAIL_IMAGE_PATH');
        $this->USER_PROFILE_PIC_WIDTH = Config::get('constant.USER_PROFILE_PIC_WIDTH');
        $this->USER_PROFILE_PIC_HEIGHT = Config::get('constant.USER_PROFILE_PIC_HEIGHT');
        
        $this->catgoryTempImage = Config::get('constant.CATEGORY_TEMP_PATH');
        
        $this->loggedInUser = Auth::guard();
        $this->log = new Logger('chats-controller');
        $this->log->pushHandler(new StreamHandler(storage_path().'/logs/monolog-'.date('m-d-Y').'.log'));
        
        $this->controller = 'ChatsController';
    }
    
    /**
     * Get New Thread Messages
     */
    public function getNewThreadMessages(Request $request)
    {   
        $headerData = $request->header('Platform');
        $outputArray = [];
        $user = JWTAuth::parseToken()->authenticate();
        $chatId = (isset($request->thread_id) && !empty($request->thread_id)) ? $request->thread_id : 0;
        try 
        {
            $filters = [];
            $filters['chat_id'] = $chatId;
            $filters['updated_at'] = 'updated_at';
            $filters['read_by_id'] = $user->id;
            $getAllChatMessages = $this->objChatMessages->getAll($filters);               
            if($getAllChatMessages && count($getAllChatMessages) > 0)
            {
                foreach($getAllChatMessages as $uKey => $uValue)
                {
                    $updateData['id'] = $uValue->id;
                    $updateData['read_by'] = $uValue->read_by.','.$user->id;
                    $updatevalue = $this->objChatMessages->insertUpdate($updateData);
                }
                $outputArray['status'] = 1;
                $outputArray['message'] = trans('apimessages.get_new_thread_messages_count_successfully');
                $statusCode = 200;
                $outputArray['data'] = array();
                $outputArray['data']['unread_count'] = count($getAllChatMessages);
                $outputArray['data']['messages'] = array();
                $i = 0;
                foreach($getAllChatMessages as $keyThread => $valueThread)
                {
                    $outputArray['data']['messages'][$i]['id'] = $valueThread->id;
                    $outputArray['data']['messages'][$i]['message'] = $valueThread->message;
                    $outputArray['data']['messages'][$i]['posted_by'] = $valueThread->posted_by;
                    $outputArray['data']['messages'][$i]['timestamp'] = strtotime($valueThread->updated_at)*1000;
                    $i++;  
                }
            }
            else
            {
                $this->log->info('API getNewThreadMessages no records found', array('login_user_id' => Auth::id()));
                $outputArray['status'] = 1;
                $outputArray['message'] = trans('apimessages.norecordsfound');
                $statusCode = 200;
                $outputArray['data'] = new stdClass();
            }
        } catch (Exception $e) {
            $this->log->error('API something went wrong while getNewThreadMessages', array('login_user_id' => Auth::id(), 'error' => $e->getMessage()));
            $outputArray['status'] = 0;
            $outputArray['message'] = $e->getMessage();
            $statusCode = 400;
            return response()->json($outputArray, $statusCode);
        }
        return response()->json($outputArray, $statusCode);
        
    }
    
    /**
     * Get Unread Threads Count
     */
    public function getUnreadThreadsCount(Request $request) 
    {
        $headerData = $request->header('Platform');
        $outputArray = [];
	$user = JWTAuth::parseToken()->authenticate();
        try 
        {
            $getAllChatThreads = $this->objChats->getUnreadThreadsCount($user->id);
            if($getAllChatThreads && $getAllChatThreads > 0)
            {
                $outputArray['status'] = 1;
                $outputArray['message'] = trans('apimessages.get_unread_threads_count_successfully');
                $statusCode = 200;
                $outputArray['data'] = array();
                $outputArray['data']['unread_count'] = $getAllChatThreads;
            }
            else
            {
                $this->log->info('API getUnreadThreadsCount no records found', array('login_user_id' => Auth::id()));
                $outputArray['status'] = 1;
                $outputArray['message'] = trans('apimessages.norecordsfound');
                $statusCode = 200;
                $outputArray['data'] = array();
                $outputArray['data']['unread_count'] = 0;
            }
        } catch (Exception $e) {
            $this->log->error('API something went wrong while getUnreadThreadsCount', array('login_user_id' => Auth::id(), 'error' => $e->getMessage()));
            $outputArray['status'] = 0;
            $outputArray['message'] = $e->getMessage();
            $statusCode = 400;
            return response()->json($outputArray, $statusCode);
	}
        return response()->json($outputArray, $statusCode);
    }
    
    /**
     * Send Enquiry Message
     */
    public function sendEnquiryMessage(Request $request)
    {
        $headerData = $request->header('Platform');
        $outputArray = [];
	$data = [];
        $user = JWTAuth::parseToken()->authenticate();
	$requestData = array_map('trim', $request->all());
	try
	{
            DB::beginTransaction();            
            $validator = Validator::make($requestData, 
                [
                    'message' => 'required',
                    'thread_id' => 'required'
                ]
            );            
            if ($validator->fails()) 
            {
                DB::rollback(); 
                $this->log->error('API validation failed while sendEnquiryMessage', array('login_user_id' => Auth::id()));
                $outputArray['status'] = 0;
                $outputArray['message'] = $validator->messages()->all()[0];
                $statusCode = 200;
                return response()->json($outputArray,$statusCode);
            }
            else
            {  
                $data['chat_id'] = $requestData['thread_id'];
                $data['message'] = $requestData['message'];
                $data['posted_by'] = $user->id;
                $data['read_by'] = $user->id;
                $messages = ChatMessages::where('chat_id', $requestData['thread_id'])->where('posted_by', $user->id)->get();
                $insertMessages = $this->objChatMessages->insertUpdate($data);
                if($insertMessages)
                {                    
                    if($messages->count() == 0) {
                        $chatThread = Chats::find($requestData['thread_id']);                        
                        $notificationData = [];
                        $notificationData['title'] = 'Inquiry Response';
                        $notificationData['message'] = 'Dear '.$chatThread->getUser->name.',  You got response for your inquiry on business '.$chatThread->getBusiness->name.'.  Find out what they said.';
                        $notificationData['type'] = '5';
                        $notificationData['thread_id'] = $requestData['thread_id'];
                        $notificationData['business_id'] = $chatThread->getBusiness->id;
                        $notificationData['business_name'] = $chatThread->getBusiness->name;
                        Helpers::sendPushNotification($chatThread->getUser->id, $notificationData);
                    }

                    DB::commit();
                    $insert = [];
                    $insert['id'] = $data['chat_id'];
                    $insert['updated_at'] = $insertMessages->updated_at;
                    $insertData = $this->objChats->insertUpdate($insert);
                    if($insertData)
                    {
                        $outputArray['status'] = 1;
                        $outputArray['message'] = trans('apimessages.send_inquiry_message_added_successfully');
                        $statusCode = 200;
                    }
                    else
                    {
                        $this->log->error('API something went wrong while sendEnquiryMessage', array('login_user_id' => Auth::id()));
                        $outputArray['status'] = 1;
                        $outputArray['message'] = trans('apimessages.default_error_msg');
                        $statusCode = 200;
                        $outputArray['data'] = array();
                    }                    
                }
                else
                {
                    DB::rollback();
                    $this->log->error('API something went wrong while sendEnquiryMessage', array('login_user_id' => Auth::id()));
                    $outputArray['status'] = 0;
                    $outputArray['message'] = trans('apimessages.default_error_msg');
                    $statusCode = 400;
                }
            }
	} catch (Exception $e) {
            DB::rollback();
            $this->log->error('API something went wrong while sendEnquiryMessage', array('login_user_id' => Auth::id(), 'error' => $e->getMessage()));
            $outputArray['status'] = 0;
            $outputArray['message'] = $e->getMessage();
            $statusCode = $e->getStatusCode();
            return response()->json($outputArray, $statusCode);
	}
	return response()->json($outputArray, $statusCode);
    }
    
    /**
     * Get Thread Messages
     */
    public function getThreadMessages(Request $request)
    {
        $headerData = $request->header('Platform');
        $outputArray = [];
        $user = JWTAuth::parseToken()->authenticate();
        $pageNo = (isset($request->page) && !empty($request->page)) ? $request->page : 1;
        $chatId = (isset($request->thread_id) && !empty($request->thread_id)) ? $request->thread_id : 0;
        try 
        {
            $chatsDetails = Chats::find($chatId);
            if($chatsDetails && count($chatsDetails) > 0)
            {
                $outputArray['status'] = 1;
                $outputArray['message'] = trans('apimessages.get_thread_messages_fetched_successfully');
                $getUnreadThreadsCount = $this->objChats->getUnreadThreadsCount($user->id);
                $outputArray['unread_count'] = $getUnreadThreadsCount;
                $statusCode = 200;
                $outputArray['data'] = array();
                $outputArray['data']['id'] = $chatsDetails->id;
                $outputArray['data']['title'] = $chatsDetails->title;
                $outputArray['data']['business_id'] = $chatsDetails->business_id;
                if($chatsDetails->customer_id == $user->id)
                {
                    $imgThumbUrl = ((isset($chatsDetails->getBusiness) && isset($chatsDetails->getBusiness->businessImagesById) && !empty($chatsDetails->getBusiness->businessImagesById->image_name)) && Storage::size(Config::get('constant.BUSINESS_THUMBNAIL_IMAGE_PATH').$chatsDetails->getBusiness->businessImagesById->image_name) > 0) ? Storage::url(Config::get('constant.BUSINESS_THUMBNAIL_IMAGE_PATH').$chatsDetails->getBusiness->businessImagesById->image_name) : url(Config::get('constant.DEFAULT_IMAGE'));

                    $outputArray['data']['image_url'] = $imgThumbUrl;
                    $outputArray['data']['name'] = (isset($chatsDetails->getBusiness) && !empty($chatsDetails->getBusiness->name)) ? $chatsDetails->getBusiness->name : '';
                    $outputArray['data']['type'] = 'business';
                }
                else
                {
                    $imgUserThumbUrl = ((isset($chatsDetails->getUser) && !empty($chatsDetails->getUser->profile_pic)) && Storage::size(Config::get('constant.USER_THUMBNAIL_IMAGE_PATH').$chatsDetails->getUser->profile_pic) > 0) ? Storage::url(Config::get('constant.USER_THUMBNAIL_IMAGE_PATH').$chatsDetails->getUser->profile_pic) : url(Config::get('constant.DEFAULT_IMAGE'));

                    $outputArray['data']['image_url'] = $imgUserThumbUrl;
                    $outputArray['data']['name'] = (isset($chatsDetails->getUser) && !empty($chatsDetails->getUser->name)) ? $chatsDetails->getUser->name : '';
                    $outputArray['data']['type'] = 'customer';
                }
                
                $filters = [];
                $filters['chat_id'] = $chatId;
                $filters['updated_at'] = 'updated_at';
                $filters['read_by_id'] = $user->id;
                $getReadByChatMessages = $this->objChatMessages->getAll($filters);               
                if(count($getReadByChatMessages) > 0)
                {
                    foreach($getReadByChatMessages as $uKey => $uValue)
                    {
                        $updateData['id'] = $uValue->id;
                        $updateData['read_by'] = $uValue->read_by.','.$user->id;
                        $updatevalue = $this->objChatMessages->insertUpdate($updateData);
                    }
                }
                if($headerData == Config::get('constant.WEBSITE_PLATFORM'))
                {
                    $filters = [];
                    $filters['chat_id'] = $chatId;
                    $filters['updated_at'] = 'updated_at';
                    $offset = Helpers::getWebChatOffset($pageNo);
                    $filters['skip'] = $offset;            
                    $filters['take'] = Config::get('constant.WEB_CHAT_RECORD_PER_PAGE');
                }
                else
                {
                    $filters = [];
                    $filters['chat_id'] = $chatId;
                    $filters['updated_at'] = 'updated_at';
                    $offset = Helpers::getMobileChatOffset($pageNo);
                    $filters['skip'] = $offset;            
                    $filters['take'] = Config::get('constant.MOBILE_CHAT_RECORD_PER_PAGE'); 
                }
                $getAllChatMessages = $this->objChatMessages->getAll($filters);
                $outputArray['loadMore'] = 0;
                $outputArray['data']['messages'] = array();
                
                if($getAllChatMessages && count($getAllChatMessages) > 0)
                {
                    if($headerData == Config::get('constant.WEBSITE_PLATFORM'))
                    {
                        if($getAllChatMessages->count() < Config::get('constant.WEB_CHAT_RECORD_PER_PAGE'))
                        {
                            $outputArray['loadMore'] = 0;
                        } else {
                            $offset = Helpers::getWebChatOffset($pageNo+1);
                            $filters = [];
                            $filters['chat_id'] = $chatId;
                            $filters['offset'] = $offset;  
                            $filters['take'] = Config::get('constant.WEB_CHAT_RECORD_PER_PAGE');
                            $chatThreadCount = $this->objChatMessages->getAll($filters);
                            $outputArray['loadMore'] = (count($chatThreadCount) > 0) ? 1 : 0 ;
                        }
                    }
                    else
                    {
                        if($getAllChatMessages->count() < Config::get('constant.MOBILE_CHAT_RECORD_PER_PAGE'))
                        {
                            $outputArray['loadMore'] = 0;
                        } else {
                            $offset = Helpers::getMobileChatOffset($pageNo+1);
                            $filters = [];
                            $filters['chat_id'] = $chatId;
                            $filters['offset'] = $offset;  
                            $filters['take'] = Config::get('constant.MOBILE_CHAT_RECORD_PER_PAGE');
                            $chatThreadCount = $this->objChatMessages->getAll($filters);
                            $outputArray['loadMore'] = (count($chatThreadCount) > 0) ? 1 : 0 ;
                        } 
                    }                    
                    
                    $i = 0;
                    foreach($getAllChatMessages as $keyThread => $valueThread)
                    {
                        $outputArray['data']['messages'][$i]['id'] = $valueThread->id;
                        $outputArray['data']['messages'][$i]['message'] = $valueThread->message;
                        $outputArray['data']['messages'][$i]['posted_by'] = $valueThread->posted_by;
                        $outputArray['data']['messages'][$i]['timestamp'] = strtotime($valueThread->updated_at)*1000;
                        $i++;  
                    }
                }
            } 
            else
            {
                $this->log->info('API getThreadMessages no records found', array('login_user_id' => Auth::id()));
                $outputArray['status'] = 1;
                $outputArray['message'] = trans('apimessages.norecordsfound');
                $outputArray['unread_count'] = 0;
                $statusCode = 200;
                $outputArray['loadMore'] = 0;
                $outputArray['data'] = array();
            }
        } catch (Exception $e) {
            $this->log->error('API something went wrong while getThreadMessages', array('login_user_id' => Auth::id(), 'error' => $e->getMessage()));
            $outputArray['status'] = 0;
            $outputArray['message'] = $e->getMessage();
            $statusCode = 400;
            return response()->json($outputArray, $statusCode);
        }
        return response()->json($outputArray, $statusCode);
    }
    
    /**
     * Get Thread Listing
     */
    public function getThreadListing(Request $request)
    {
        $headerData = $request->header('Platform');
        $outputArray = [];
        $user = JWTAuth::parseToken()->authenticate();
        $pageNo = (isset($request->page) && !empty($request->page)) ? $request->page : 1;
        try 
        {
            if(isset($user->id) && !empty($user->id))
            {
                $filters = [];
                if($headerData == Config::get('constant.WEBSITE_PLATFORM'))
                {
                    $offset = Helpers::getWebOffset($pageNo);
                    $filters['updated_at'] = 'updated_at';
                    $filters['userId'] = $user->id;
                    $filters['skip'] = $offset;            
                    $filters['take'] = Config::get('constant.WEBSITE_RECORD_PER_PAGE');
                }
                else
                {
                    $filters['userId'] = $user->id;
                    $filters['updated_at'] = 'updated_at';
                    $offset = Helpers::getOffset($pageNo);
                    $filters['skip'] = $offset;            
                    $filters['take'] = Config::get('constant.API_RECORD_PER_PAGE'); 
                }
                $getAllChatThreads = $this->objChats->getAll($filters);

                if($getAllChatThreads && count($getAllChatThreads) > 0)
                {
                    $outputArray['status'] = 1;
                    $outputArray['message'] = trans('apimessages.threads_fetched_successfully');
                    $getUnreadThreadsCount = $this->objChats->getUnreadThreadsCount($user->id);
                    $outputArray['unread_count'] = $getUnreadThreadsCount;
                    $statusCode = 200;

                    if($headerData == Config::get('constant.WEBSITE_PLATFORM'))
                    {
                        if($getAllChatThreads->count() < Config::get('constant.WEBSITE_RECORD_PER_PAGE'))
                        {
                            $outputArray['loadMore'] = 0;
                        } else{
                            $offset = Helpers::getWebOffset($pageNo+1);
                            $filters = [];
                            $filters['userId'] = $user->id;
                            $filters['updated_at'] = 'updated_at';
                            $filters['offset'] = $offset;  
                            $filters['take'] = Config::get('constant.WEBSITE_RECORD_PER_PAGE');
                            $chatThreadCount = $this->objChats->getAll($filters);
                            $outputArray['loadMore'] = (count($chatThreadCount) > 0) ? 1 : 0 ;
                        }
                    }
                    else
                    {
                        if($getAllChatThreads->count() < Config::get('constant.API_RECORD_PER_PAGE'))
                        {
                            $outputArray['loadMore'] = 0;
                        } else{
                            $offset = Helpers::getOffset($pageNo+1);
                            $filters = [];
                            $filters['userId'] = $user->id;
                            $filters['updated_at'] = 'updated_at';
                            $filters['offset'] = $offset;  
                            $filters['take'] = Config::get('constant.API_RECORD_PER_PAGE');
                            $chatThreadCount = $this->objChats->getAll($filters);
                            $outputArray['loadMore'] = (count($chatThreadCount) > 0) ? 1 : 0 ;
                        } 
                    }                
                    $outputArray['data'] = array();
                    $i = 0;
                    foreach($getAllChatThreads as $keyThreads => $valueThreads)
                    {
                        $outputArray['data'][$i]['id'] = $valueThreads->id;
                        $outputArray['data'][$i]['title'] = $valueThreads->title;

                        if($valueThreads->customer_id == $user->id)
                        {
                            // if(isset($valueThreads->getBusiness) && isset($valueThreads->getBusiness->businessImagesById) && !empty($valueThreads->getBusiness->businessImagesById->image_name))
                            // {
                            //     $imgThumbPath = $this->BUSINESS_THUMBNAIL_IMAGE_PATH.$valueThreads->getBusiness->businessImagesById->image_name;                       $imgThumbUrl = (!empty($imgThumbPath)) ? url($imgThumbPath) : url($this->catgoryTempImage);
                            // }
                            $imgThumbUrl = ((isset($valueThreads->getBusiness) && isset($valueThreads->getBusiness->businessImagesById) && !empty($valueThreads->getBusiness->businessImagesById->image_name)) && Storage::size(Config::get('constant.BUSINESS_THUMBNAIL_IMAGE_PATH').$valueThreads->getBusiness->businessImagesById->image_name) > 0) ? Storage::url(Config::get('constant.BUSINESS_THUMBNAIL_IMAGE_PATH').$valueThreads->getBusiness->businessImagesById->image_name) : url(Config::get('constant.DEFAULT_IMAGE'));

                            $outputArray['data'][$i]['image_url'] = $imgThumbUrl;
                            $outputArray['data'][$i]['name'] = (isset($valueThreads->getBusiness) && !empty($valueThreads->getBusiness->name)) ? $valueThreads->getBusiness->name : '';
                        }
                        else
                        {
                            // if(isset($valueThreads->getUser) && !empty($valueThreads->getUser->profile_pic))
                            // {
                            //     $imgThumbPath = $this->USER_THUMBNAIL_IMAGE_PATH.$valueThreads->getUser->profile_pic;
                            //     $imgUserThumbUrl = (!empty($imgThumbPath)) ? url($imgThumbPath) : url($this->catgoryTempImage);
                            // }
                            $imgUserThumbUrl = ((isset($valueThreads->getUser) && !empty($valueThreads->getUser->profile_pic)) && Storage::size(Config::get('constant.USER_THUMBNAIL_IMAGE_PATH').$valueThreads->getUser->profile_pic) > 0) ? Storage::url(Config::get('constant.USER_THUMBNAIL_IMAGE_PATH').$valueThreads->getUser->profile_pic) : url(Config::get('constant.DEFAULT_IMAGE'));
                            
                            $outputArray['data'][$i]['image_url'] = $imgUserThumbUrl;
                            $outputArray['data'][$i]['name'] = (isset($valueThreads->getUser) && !empty($valueThreads->getUser->name)) ? $valueThreads->getUser->name : '';
                        }

                        if(isset($valueThreads->getChatMessages) && count($valueThreads->getChatMessages) > 0)
                        {
                            $unreadCount = $valueThreads->getChatMessages()->whereRaw("NOT FIND_IN_SET(".$user->id.", read_by)")->count();
                        }

                        $outputArray['data'][$i]['unread_count'] = (isset($unreadCount) && $unreadCount > 0) ? $unreadCount : 0;
                        if(isset($valueThreads->getChatMessages) && count($valueThreads->getChatMessages) > 0)
                        {
                            $lastMessageData = $valueThreads->getChatMessages()->limit(1)->orderBy('updated_at', 'DESC')->first();
                            if($lastMessageData)
                            {
                                $outputArray['data'][$i]['last_message'] = array();
                                $outputArray['data'][$i]['last_message']['id'] = $lastMessageData->id;
                                $outputArray['data'][$i]['last_message']['message'] = $lastMessageData->message;
                                $outputArray['data'][$i]['last_message']['timestamp'] = strtotime($lastMessageData->updated_at)*1000;
                            }
                            else
                            {
                                $outputArray['data'][$i]['last_message'] = new stdClass();
                            }
                        }
                        else
                        {
                            $outputArray['data'][$i]['last_message'] = new stdClass();
                        }
                        $i++;  
                    }
                }
                else
                {
                    $this->log->info('API getThreadListing no records found', array('login_user_id' => Auth::id()));
                    $outputArray['status'] = 1;
                    $outputArray['message'] = trans('apimessages.norecordsfound');
                    $outputArray['unread_count'] = 0;
                    $statusCode = 200;
                    $outputArray['data'] = array();
                }  
            }
            else
            {
                $this->log->error('API something went wrong while getThreadListing', array('login_user_id' => Auth::id()));
                $outputArray['status'] = 1;
                $outputArray['message'] = trans('apimessages.invalid_user');
                $statusCode = 200;
                $outputArray['data'] = array();
            } 
                      
        } catch (Exception $e) {
            $this->log->error('API something went wrong while getThreadListing', array('login_user_id' => Auth::id(), 'error' => $e->getMessage()));
            $outputArray['status'] = 0;
            $outputArray['message'] = $e->getMessage();
            $statusCode = 400;
            return response()->json($outputArray, $statusCode);
        }
        return response()->json($outputArray, $statusCode);
    }
    
     /**
     * Send Enquiry
     */
    public function sendEnquiry(Request $request)
    {
        $headerData = $request->header('Platform');
        $outputArray = [];
	$data = [];
        $user = JWTAuth::parseToken()->authenticate();        
	$requestData = array_map('trim', $request->all());
	try
	{
            DB::beginTransaction();            
            $validator = Validator::make($requestData, 
            [
                'title' => 'required',
                'business_id' => 'required'
            ]
            );            
            if ($validator->fails()) 
            {
                DB::rollback(); 
                $this->log->error('API validation failed while sendEnquiry', array('login_user_id' => Auth::id()));
                $outputArray['status'] = 0;
                $outputArray['message'] = $validator->messages()->all()[0];
                $statusCode = 200;
                return response()->json($outputArray,$statusCode);
            }
            else
            {  
                $businessId = $requestData['business_id'];
                $getBusinessData = Business::find($businessId);   
                if($getBusinessData && count($getBusinessData) > 0)
                {
                    $data['title'] = $requestData['title'];
                    $data['business_id'] = $businessId;
                    $data['customer_id'] = $user->id;
                    $data['member_id'] = $getBusinessData->user_id;
                    $insertData = $this->objChats->insertUpdate($data);
                    if($insertData)
                    {
                        $insert = [];
                        $insert['chat_id'] = $insertData->id;
                        $insert['message'] = $requestData['message'];
                        $insert['posted_by'] = $user->id;
                        $insert['read_by'] = $user->id;
                        $insertMessages = $this->objChatMessages->insertUpdate($insert);
                        if($insertMessages)
                        {
                            //Send push notification to Business User
                            $notificationData = [];
                            $notificationData['title'] = 'New Enquiry Recieved';
                            $notificationData['message'] = 'Dear '.$getBusinessData->user->name.',  You have got an inquiry from '.$user->name.' '.$user->phone.' on your business.';
                            $notificationData['type'] = '2';
                            $notificationData['thread_id'] = $insertData->id;
                            $notificationData['business_id'] = $getBusinessData->id;
                            $notificationData['business_name'] = $getBusinessData->name;
                            Helpers::sendPushNotification($getBusinessData->user_id, $notificationData);

                            DB::commit();
                            $outputArray['status'] = 1;
                            $outputArray['message'] = trans('apimessages.send_inquiry_added_successfully');
                            $statusCode = 200;
                        }
                        else
                        {
                            DB::rollback();
                            $this->log->error('API something went wrong while sendEnquiry', array('login_user_id' => Auth::id()));
                            $outputArray['status'] = 0;
                            $outputArray['message'] = trans('apimessages.default_error_msg');
                            $statusCode = 400;
                        }
                    }
                    else
                    {
                        DB::rollback();
                        $this->log->error('API something went wrong while sendEnquiry', array('login_user_id' => Auth::id()));
                        $outputArray['status'] = 0;
                        $outputArray['message'] = trans('apimessages.default_error_msg');
                        $statusCode = 400;
                    }
                }
                else
                {
                    $this->log->info('API sendEnquiry no records found', array('login_user_id' => Auth::id()));
                    $outputArray['status'] = 1;
                    $outputArray['message'] = trans('apimessages.no_business_recordsfound');
                    $statusCode = 200;
                    $outputArray['data'] = array();
                }
            }
	} catch (Exception $e) {
            DB::rollback();
            $this->log->error('API something went wrong while sendEnquiry', array('login_user_id' => Auth::id()));
            $outputArray['status'] = 0;
            $outputArray['message'] = $e->getMessage();
            $statusCode = $e->getStatusCode();
            return response()->json($outputArray, $statusCode);
	}
	return response()->json($outputArray, $statusCode);
    }
    
   
}
