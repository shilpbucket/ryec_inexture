@extends('Admin.Master')

@section('content')
<!-- content push wrapper -->

<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>

        {{trans('labels.users')}}
        <div class="pull-right">
            <a href="{{ url('admin/adduser') }}" class="btn bg-purple ">
                <i class="fa fa-plus"></i>&nbsp;{{trans('labels.addbtn')}} {{trans('labels.user')}}
            </a>
            <a href="{{ url('admin/addagent') }}" class="btn bg-purple">
                <i class="fa fa-plus"></i>&nbsp;{{trans('labels.addbtn')}} {{trans('labels.agent')}}
            </a>
        </div>
    </h1>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header">
                    <form id="formSearch" class="form-horizontal" method="post" action="{{ url('/admin/users') }}">
                        <div class="col-md-4">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <input type="hidden" name="search" value="1">
                            <input type="text" value="{{(isset($postData['searchtext'])) ? $postData['searchtext'] : ''}}" name="searchtext" class="form-control" placeholder="search text">
                        </div>
                        <div class="col-md-2">
                           <select class="form-control" name="usertype">
                                <option value=''>{{trans('labels.selecttype')}}</option>
                                <option value='vendor' {{(isset($postData['usertype']) && $postData['usertype'] == 'vendor')?'selected':''}}>{{trans('labels.vendor')}}</option>
                                <option value='agent' {{(isset($postData['usertype']) && $postData['usertype'] == 'agent')?'selected':''}}>{{trans('labels.agent')}}</option>
                                <option value='deactive' {{(isset($postData['usertype']) && $postData['usertype'] == 'deactive')?'selected':''}}>{{trans('labels.deactive')}}</option>
                                <option value='active' {{(isset($postData['usertype']) && $postData['usertype'] == 'active')?'selected':''}}>{{trans('labels.active')}}</option>
                                <option value='deleted' {{(isset($postData['usertype']) && $postData['usertype'] == 'deleted')?'selected':''}}>{{trans('labels.deleted')}}</option>
                           </select>
                        </div>
                        <div class="col-md-2">
                            <input type="submit" class="btn bg-purple" name="searchBtn" id="searchBtn" value="{{trans('labels.search')}}"/>
                            <a href="{{ url('/admin/users') }}">
                                <input type="button" class="btn bg-purple" name="clearBtn" id="clearBtn" value="{{trans('labels.clear')}}"/>
                            </a>
                        </div>
                    </form>
                </div>
                <div class="box-body">
                    <table class="table table-bordered table-striped" id="users">
                        <thead>
                            <tr>
                                <th>{{trans('labels.id')}}</th>
                                <th>{{trans('labels.name')}}</th>
                                <th>{{trans('labels.email')}}</th>
                                <th>{{trans('labels.birthdate')}}</th>
                                <th>{{trans('labels.phone')}}</th>
                                <th>{{trans('labels.memberoragent')}}</th>
                                <th>{{trans('labels.photo')}}</th>
                                <th>{{trans('labels.headeraction')}}</th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse($userList as $key=>$value)
                            <tr>
                                <td>
                                    {{$value->id}}
                                </td>
                                <td>
                                    {{$value->name}}
                                </td>
                                <td>
                                    {{strip_tags($value->email)}}
                                </td>
                                <td>
                                    {{$value->dob}}
                                </td>
                                <td>
                                    @if($value->country_code)
                                        ({{$value->country_code}}){{$value->phone}}
                                    @else
                                        {{$value->phone}}
                                    @endif
                                </td>
                                <td>
                                    <?php $isVendor = Helpers::userIsVendorOrNot($value->id); ?>
                                    @if($isVendor == 1)
                                        <span class="label label-success">{{trans('labels.vendor')}}</span>
                                    @endif

                                    @if($value->agent_approved == 1)
                                        <span class="label label-success">{{trans('labels.agent')}}</span>
                                    @endif
                                </td>
                                <td>
                                    @if($value->profile_pic != '' && Storage::size(Config::get('constant.USER_THUMBNAIL_IMAGE_PATH').$value->profile_pic) > 0)
                                        <img style="cursor: pointer;" data-toggle='modal' data-target='#{{$value->id.substr(trim($value->profile_pic), 0, -10)}}' src="{{ Storage::url(Config::get('constant.USER_THUMBNAIL_IMAGE_PATH').$value->profile_pic) }}" width="50" height="50" class="img-circle"/>
                                        <div class='modal modal-centered fade image_modal' id='{{$value->id.substr(trim($value->profile_pic), 0, -10)}}' role='dialog' style='vertical-align: center;'>
                                            <div class='modal-dialog modal-dialog-centered'>
                                                <div class='modal-content' style="background-color:transparent;">
                                                    <div class='modal-body'>
                                                    <center>
                                                        <button type='button' class='close' data-dismiss='modal'>&times;</button>
                                                        <img src="{{ Storage::url(Config::get('constant.USER_ORIGINAL_IMAGE_PATH').$value->profile_pic) }}" style='width:100%; border-radius:5px;' title="{{$value->profile_pic}}" />
                                                    <center>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @else
                                        <img src="{{ url('images/default.png') }}" width="50" height="50" class="img-circle"/>
                                    @endif
                                </td>
                                <td>
                                    @if(isset($postData['usertype']) && $postData['usertype'] == 'deleted')
                                        <!-- <a href="{{ url('/admin/activate')}}/{{$value->id}}"> -->
                                            <button  class="btn btn-success" onclick="activate({{$value->id}})">
                                                <i class="fa fa-check"></i>&nbsp;Acivate
                                            </button>
                                        <!-- </a> -->
                                    @else
                                        @if($value->agent_approved == 1)
                                            <a href="{{ url('/admin/editagent') }}/{{Crypt::encrypt($value->id)}}">
                                                <span data-toggle="tooltip" data-original-title="Edit" class='glyphicon glyphicon-edit'></span>
                                            </a>&nbsp;&nbsp;
                                        @else
                                            <a href="{{ url('/admin/edituser') }}/{{Crypt::encrypt($value->id)}}">
                                                <span data-toggle="tooltip" data-original-title="Edit" class='glyphicon glyphicon-edit'></span>
                                            </a>&nbsp;&nbsp;
                                        @endif
                                        <a onclick="return confirm('Are you sure you want to delete ?')" href="{{ url('/admin/deleteuser') }}/{{$value->id}}">
                                            <span data-toggle="tooltip" data-original-title="Delete" class='glyphicon glyphicon-remove'></span>
                                        </a>&nbsp;&nbsp;

                                        <a href="{{ url('/admin/user/business') }}/{{Crypt::encrypt($value->id)}}">
                                            <span data-toggle="tooltip" class="badge bg-light-blue" data-original-title="Manage Business" style="margin-bottom: 3px;">B</span>
                                        </a>
                                    @endif
                                </td>
                            </tr>
                            @empty
                            <tr>
                                <th colspan="8"><center>No records found</center></td>
                            </tr>
                            @endforelse
                        </tbody>
                    </table>

                    @if (isset($userList) && !empty($userList))
                        <div class="pull-right">
                            @if(isset($postData['searchtext']) && $postData['searchtext'] != '')
                                <?php
                                    $searchtext = $postData['searchtext'];
                                ?>
                            @else
                                <?php $searchtext = ''; ?>
                            @endif
                            @if(isset($postData['usertype']) && $postData['usertype'] != '')
                                <?php
                                    $usertype = $postData['usertype'];
                                ?>
                            @else
                                <?php $usertype = ''; ?>
                            @endif
                            <?php echo $userList->appends(['searchtext' => $searchtext, 'usertype' => $usertype])->render(); ?>
                        </div>
                    @endif
                </div><!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div><!-- /.col -->
    </div><!-- /.row -->
</section><!-- /.content -->
@stop
@section('script')
<script type="text/javascript">
    $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
      checkboxClass: 'icheckbox_flat-green',
      radioClass   : 'iradio_flat-green'
    })

    function activate(userId)
    {
        var token = '<?php echo csrf_token() ?>';
        $.ajax({
            headers: { 'X-CSRF-TOKEN': token },
            type: "GET",
            url: "{{url('/admin/activate')}}/"+userId,
            success: function( data ) {
                location.reload();
                $('#alert_dashboard').show();
            }
        });
    }
</script>
@stop