@extends('Admin.Master')

@section('content')
<!-- content   -->

<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        {{trans('labels.dashboard')}}
        <small>Control Panel</small>
    </h1>
</section>

<section class="content">
    <div class="alert alert-success alert-dismissable" id="alert_dashboard" style="display:none;">
        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">X</button>
        <h4><i class="icon fa fa-check"></i> Success!</h4>
        Business approved successfully
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">List of members waiting for approval</h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    </div>
                </div>
                <div class="box-body">
                    <table id="membersForApproval" class='table table-bordered table-striped'>
                        <thead>
                            <tr>
                                <th>{{trans('labels.user')}}</th>
                                <th>{{trans('labels.phone')}}</th>
                                <th>{{trans('labels.business')}}</th>
                                <th>{{trans('labels.mobile')}}</th>
                                <th>{{trans('labels.address')}}</th>
                                <th>{{trans('labels.email')}}</th>
                                <th>{{trans('labels.headeraction')}}</th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse($membersForApproval as $member)
                            <tr>
                                <td>{{(isset($member->user))?$member->user->name:""}}</td>
                                <td>{{(isset($member->user))?$member->user->phone:""}}</td>
                                <td>{{$member->name}}</td>
                                <td>{{$member->mobile}}</td>
                                <td>{{$member->address}}</td>
                                <td>{{$member->email_id}}</td>
                                <td>
                                    <div class="business_approve">
                                        <button class="btn btn-success" onclick="approved({{$member->id}})" >
                                            <i class="fa fa-check"></i>&nbsp;Approve
                                        </button>
                                    </div>
                                </td>
                            </tr>
                            @empty
                            @endforelse
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">List of Subscriptions Expired/ Expiring</h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    </div>
                </div>
                <div class="box-body">
                    To be implemented
                </div>
            </div>
        </div>
    </div>
</section>
@stop
@section('script')
<script type="text/javascript">
    $(document).ready(function() {
        $('#membersForApproval').DataTable({
           "aaSorting": []
        });
    });

    function approved(businessId)
    {
        var token = '<?php echo csrf_token() ?>';
        $.ajax({
            headers: { 'X-CSRF-TOKEN': token },
            type: "POST",
            url: "{{url('/admin/user/business/approved')}}",
            data: {businessId: businessId},
            success: function( data ) {
                location.reload();
                $('#alert_dashboard').show();
            }
        });
    }
</script>
@stop
