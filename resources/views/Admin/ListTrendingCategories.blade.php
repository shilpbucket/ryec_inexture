@extends('Admin.Master')

@section('content')

<!-- Content Wrapper. Contains page content -->

<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        {{trans('labels.trending_categories')}}
    </h1>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <!-- right column -->
        <div class="col-md-12">
            <!-- Horizontal Form -->
            <div class="box">
                @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">X</button>
                    <strong>{{trans('labels.whoops')}}</strong> {{trans('labels.someproblems')}}<br><br>
                    <ul>
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                @endif
                <div class="box-body">
                    <table id="trendingCategoriesListing" class='table table-bordered table-striped'>
                        <thead>
                            <tr>
                                <th>{{trans('labels.id')}}</th>
                                <th>{{trans('labels.lblcategoryname')}}</th>
                                <th>{{trans('labels.catlogo')}}</th>
                                <th>{{trans('labels.bannerimage')}}</th>
                                <th>{{trans('labels.headerstatus')}}</th>
                                <th>{{trans('labels.headeraction')}}</th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse($getTrendingCategories as $key=>$value)
                            <tr>
                                <td>{{$value->id}}</td>

                                <td>{{$value->name}}</td>
                                
                                <td>
                                    @if(File::exists(public_path(Config::get('constant.CATEGORY_LOGO_THUMBNAIL_IMAGE_PATH').$value->cat_logo)) && $value->cat_logo != NULL && !empty($value->cat_logo))
                                    <img style="cursor: pointer;" data-toggle='modal' data-target='#{{$value->id.substr(trim($value->cat_logo), 0, -10)}}' src="{{ asset(Config::get('constant.CATEGORY_LOGO_THUMBNAIL_IMAGE_PATH').$value->cat_logo) }}" height="40" width="40" title="{{$value->cat_logo}}" class="img-circle"/>
                                    <div class='modal modal-centered fade image_modal' id='{{$value->id.substr(trim($value->cat_logo), 0, -10)}}' role='dialog' style='vertical-align: center;'>
                                        <div class='modal-dialog modal-dialog-centered'>
                                            <div class='modal-content' style="background-color:transparent;">
                                                <div class='modal-body'>
                                                <center>
                                                    <button type='button' class='close' data-dismiss='modal'>&times;</button>
                                                    <img src="{{ asset(Config::get('constant.CATEGORY_LOGO_ORIGINAL_IMAGE_PATH').$value->cat_logo) }}" style='max-height:680px; border-radius:5px;' title="{{$value->cat_logo}}" class="img-circle"/>
                                                <center>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @else
                                    -
                                @endif
                                </td>
                                
                                <td>
                                    @if( File::exists(public_path(Config::get('constant.CATEGORY_BANNER_ORIGINAL_IMAGE_PATH').$value->banner_img)) && $value->banner_img != NULL && !empty($value->banner_img) )
                                    <img style="cursor: pointer;" data-toggle='modal' data-target='#{{$value->id.substr(trim($value->banner_img), 0, -10)}}' src="{{ asset(Config::get('constant.CATEGORY_BANNER_ORIGINAL_IMAGE_PATH').$value->banner_img) }}" height="40" width="40" title="{{$value->banner_img}}" />
                                    <div class='modal modal-centered fade image_modal' id='{{$value->id.substr(trim($value->banner_img), 0, -10)}}' role='dialog' style='vertical-align: center;'>
                                        <div class='modal-dialog modal-dialog-centered'>
                                            <div class='modal-content' style="background-color:transparent;">
                                                <div class='modal-body'>
                                                <center>
                                                    <button type='button' class='close' data-dismiss='modal'>&times;</button>
                                                    <img src="{{ asset(Config::get('constant.CATEGORY_BANNER_ORIGINAL_IMAGE_PATH').$value->banner_img) }}" style='width:100%; border-radius:5px;' title="{{$value->cat_logo}}" />
                                                <center>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @else
                                    -
                                @endif
                                </td>
                                
                                <td id="trendingStatus_{{$value->id}}">
                                    @if ($value->trending_category == 1)
                                        <span class="label label-success">Trending</span>
                                    @else
                                        -
                                    @endif
                                </td>
                                <td>
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" name="trendingCategories[]" id="trendingCategory_{{$value->id}}" onclick="updateTrendingCategory(this.value)" value="{{$value->id}}" <?php echo ($value->trending_category == 1) ? 'checked' : '' ?>>
                                        </label>
                                    </div>
                                </td>
                            </tr>
                            @empty
                            @endforelse

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section><!-- /.content -->
@stop
@section('script')
<script type="text/javascript">
    var table;
    $(document).ready(function() {
        table = $('#trendingCategoriesListing').DataTable({
           'aaSorting': [],
           'order': [[ 3, "desc" ]],
           'paging': false
        });
    });
</script>

<script type="text/javascript">
    
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    
    function updateTrendingCategory(categroyId)
    {
        var checkBox = document.getElementById("trendingCategory_"+categroyId);
        var trendingCategory = 0
        if (checkBox.checked == true)
        {
            trendingCategory = 1;            
        }  
        $.ajax({
            type: 'post',
            url: '{{ url("admin/updateTrendingCategory") }}',
            data: {
                categoryId: categroyId,
                trendingCategory:trendingCategory
            },
            success: function (response)
            {
                if(response !== '' && response != 0)
                {
                    if(trendingCategory == 1) {
                        $('#trendingStatus_'+categroyId).html('<span class="label label-success">Trending</span>');
                    } else{
                        $('#trendingStatus_'+categroyId).html('-');
                    } 
                }
            }
        });
    }
</script>

@stop
