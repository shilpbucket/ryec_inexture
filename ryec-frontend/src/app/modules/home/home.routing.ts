import { Routes, RouterModule } from '@angular/router';
import { ModuleWithProviders } from '@angular/core';
import {
    HomeComponent, BusinessListComponent,
    HomeDashboardComponent, AddBusinessComponent, AgentBusinessListComponent,
    BusinessDetailComponent, BusinessProfileComponent, MyBusinessDetailComponent, InvestmentOpportunityComponent, 
    InvestmentOpportunityDetailComponent, BusinessSearchComponent, CreateInvestmentOpportunityComponent, 
    MyInvestmentOpportunityDetailComponent, MembershipPlanComponent } from '../../components';
import { AuthGuard } from './../../guards/auth-guard.service';

export const routes: Routes = [
    {
        path: '',
        component: HomeComponent,
        canActivate: [AuthGuard],
        canLoad: [AuthGuard],
        canActivateChild: [AuthGuard],
        children: [
            { path: '', component: HomeDashboardComponent, data: { title: 'RYEC - Dashboard' } },
            { path: 'home/add-business', component: AddBusinessComponent, data: { title: 'RYEC - Business Register' } },
            { path: 'home/business-list/:slug/:slug', component: BusinessListComponent, data: { title: 'RYEC - Business List' } },
            { path: 'home/business-detail/:slug', component: BusinessDetailComponent, data: { title: 'RYEC - Business List' } },
            { path: 'home/business-profile/:slug', component: BusinessProfileComponent, data: { title: 'RYEC - Business Profile' } },
            { path: 'home/my-business-detail', component: MyBusinessDetailComponent, data: { title: 'RYEC - My Business Profile' } },
            { path: 'home/agent-business-list/:slug', component: AgentBusinessListComponent, data: { title: 'RYEC - Business List' } },
            { path: 'home/investment-opportunity/:slug', component: InvestmentOpportunityComponent, data: { title: 'RYEC - Investment Opportunity List'}},
            { path: 'home/investment-opportunity-detail/:slug', component: InvestmentOpportunityDetailComponent, data: { title: 'RYEC - Investment Opportunity Detail'}},
            { path: 'home/business-search/:city/:search/:slug', component: BusinessSearchComponent, data: { title: 'RYEC - Business Search List'}},
            { path: 'home/create-investment-opportunity/:slug', component: CreateInvestmentOpportunityComponent, data: { title: 'RYEC - Create Investment Opportunity'}},
            { path: 'home/my-investment-opportunity-detail/:slug', component: MyInvestmentOpportunityDetailComponent, data: { title: 'RYEC - Investment Opportunity Detail'}},
            { path: 'home/membership-plan', component: MembershipPlanComponent, data: { title: 'RYEC - Membership Plans'}}
        ]
    }
];

export const Routing: ModuleWithProviders = RouterModule.forChild(routes);