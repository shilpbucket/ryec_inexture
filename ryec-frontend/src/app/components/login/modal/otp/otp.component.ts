import { Component, OnInit, ViewEncapsulation, Input } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { environment } from '../../../../../environments/environment';
import { HttpService, CommonService } from '../../../../services';
import { Router } from '@angular/router';

@Component({
	selector: 'ryec-otp',
	templateUrl: './otp.component.html',
	styleUrls: ['./otp.component.css'],
	encapsulation: ViewEncapsulation.None
})
export class OtpComponent implements OnInit {

	@Input() mobile = 0;
	@Input() country_code = 0;

	otpForm: FormGroup;

	constructor(private fb: FormBuilder,
		public activeModal: NgbActiveModal,
		private router: Router,
		private cS: CommonService,
		private hS: HttpService) {

		this.otpForm = this.fb.group({
			phone: [null],
			reset_password_otp: [null, Validators.compose([Validators.required,
			Validators.maxLength(6),
			Validators.minLength(6),
			Validators.pattern('^[0-9]+$')])],
			country_code: []
		});
	}


	ngOnInit() {
	}

	/**
	 * resend OTP again
	 */
	resendOTP(): void {
		const uri = environment.RYEC_API_URL + 'user/resetpasswordrequest';
		this.hS.post(uri, { phone: this.mobile }).subscribe((data: any) => {
			this.cS.showSuccess(data.message);
		}, err => {
			console.log(err);
		});
	}

	/**
	 * OTP verification
	 */
	otpCheck() {
		if (this.otpForm.valid) {
			this.otpForm.value.phone = this.mobile;
			this.otpForm.value.country_code = this.country_code;

			const uri = environment.RYEC_API_URL + 'user/resetpasswordrequestconfirm';
			this.hS.post(uri, this.otpForm.value).subscribe((res: any) => {
				if (res.status === 1) {
					// dismiss active model
					this.activeModal.dismiss();
					console.log(res);

					const tokenMix = btoa(res.data.reset_password_otp + ':' + this.otpForm.value.phone + ':' + this.otpForm.value.country_code);
					this.router.navigate(['account-recovery'], { queryParams: { token: tokenMix } });
				}
			}, err => {
				console.log(err);
			});
		} else {
			this.validateAllFields(this.otpForm);
		}
	}

	validateAllFields(Fg: FormGroup) {
		Object.keys(Fg.controls).forEach(field => {
			const control = Fg.get(field);
			if (control instanceof FormControl) {
				control.markAsTouched({ onlySelf: true });
			} else if (control instanceof FormGroup) {
				this.validateAllFields(control);
			}
		});
	}
}