export const environment = {
  production: true,
  RYEC_API_URL: 'http://ryecbackend.inexture.com/api/',
  NDA: 'No Data Available',
  GOOGLE_MAP_API: 'https://maps.google.com/maps/api/js?key=AIzaSyCTdAmyxsAISAcV_jnGdndkmaWGWI1J6to&libraries=places',
  BUSINESS_LIST_LIMIT: 10,
  LOCATION_RADIUS: 25,
  DESCRIPTIO_TRIM_LENGTH: 500
};
